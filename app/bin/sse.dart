import "dart:async";
import 'dart:convert';

import 'package:eventsource/publisher.dart';
import 'package:shelf/shelf.dart';
import "package:shelf/shelf_io.dart" as io;
import 'package:shelf_eventsource/shelf_eventsource.dart';
import 'package:shelf_router/shelf_router.dart';

import 'lat_long.dart';

main() {
  var app = Router();

  app.get("/events", _handlerEventsWithoutUser);
  app.get("/events/<user>", _handlerEventsWithtUser);
  app.get("/test/<user>", _handlerLuan);

  io.serve(app, '0.0.0.0', 80);
}

_handlerEventsWithoutUser(dynamic r) {
  final publisher = EventSourcePublisher();
  generateEvents(publisher);
  var handler = eventSourceHandler(publisher);
  handler(r);
}

_handlerEventsWithtUser(dynamic request, String user) {
  final publisher = EventSourcePublisher();
  generateEvents(publisher, user: user);
  var handler = eventSourceHandler(publisher, channel: user);
  handler(request);
}

Response _handlerLuan(Request request, String user) {
  final String response = 'Olá, $user';
  return Response.ok(response);
}

generateEvents(
  EventSourcePublisher publisher, {
  String? user,
}) {
  int id = 0;

  final List<LatLong> locations = [
    LatLong(latitude: -23.161639741542583, longitude: -45.89616988344225),
    LatLong(latitude: -23.164867219908086, longitude: -45.89411797323012),
    LatLong(latitude: -23.165242045378182, longitude: -45.893871209996846),
    LatLong(latitude: -23.1657549627423, longitude: -45.89365663327227),
    LatLong(latitude: -23.165902919309115, longitude: -45.89360298909112),
    LatLong(latitude: -23.166041011957336, longitude: -45.893560073746194),
    LatLong(latitude: -23.166267878141664, longitude: -45.893431327711454),
    LatLong(latitude: -23.16664269969099, longitude: -45.89331331051294),
    LatLong(latitude: -23.166997792822492, longitude: -45.89314164913327),
    LatLong(latitude: -23.167490976165073, longitude: -45.89298071658984),
    LatLong(latitude: -23.161639741542583, longitude: -45.89616988344225),
  ];
  Timer.periodic(const Duration(seconds: 2), (timer) {
    bool finished = id >= 10;

    final data = json.encode({
      'id': id,
      'device': 'Rastreador $id',
      'latitude': locations[id].latitude,
      'longitude': locations[id].longitude,
      'finished': finished,
      'user': user,
      'timer': timer.tick,
    });

    publisher.add(Event(data: data), channels: [user ?? '']);

    if (finished) {
      timer.cancel();

      publisher.close();
    }

    id++;
  });
}
